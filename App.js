
import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MenuPrincipal from './src/layout/menuPrincipal'; 
import Count from './src/components/count';
import Carrosel from './src/components/carrosel';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={MenuPrincipal} options={{ headerShown: false }} />
        <Stack.Screen name="Count" component={Count} />
        <Stack.Screen name="Carrosel" component={Carrosel} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
});
