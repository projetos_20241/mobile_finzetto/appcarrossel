import React, { useRef, useState } from "react";
import { View, Text, Dimensions, PanResponder, Image, StyleSheet } from "react-native";
import Count from "./count";

const images = [
    
    'https://postgrain.com/wp-content/uploads/2017/04/burst.jpg',
    'https://tntsports.com.br/__export/1691866008025/sites/esporteinterativo/img/2023/08/12/rojas_-_gazeta-press-foto-1906103.jpg_423682103.jpg',
    'https://istoe.com.br/wp-content/uploads/2023/09/pin5403-crop-galeria.jpg',
    
];

export default function ImageSlider() {
    const [cont, setCount] = useState(0);
    const screenHeight = Dimensions.get("window").height;
    const gestureThreshold = screenHeight * 0.25;

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gestureState) => {
                
            },
            onPanResponderRelease: (event, gestureState) => {
                if (gestureState.dx < -gestureThreshold) {
                    
                    setCount((prevCount) => (prevCount > 0 ? prevCount - 1 : prevCount));
                } else if (gestureState.dx > gestureThreshold) {
                    
                    setCount((prevCount) => (prevCount < images.length - 1 ? prevCount + 1 : prevCount));
                }
            },
        })
    ).current;

    return (
        <View
            {...panResponder.panHandlers}
            style={styles.container}
        >
            <Image
                source={{ uri: images[cont] }}
                style={styles.image}
            />
            <Text style={styles.text}>Imagem {cont + 1}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#f0f0f0",
    },
    image: {
        width: 200,
        height: 200,
        resizeMode: 'cover',
        marginBottom: 20,
    },
    text: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#333",
    },
});
