import React from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function MenuPrincipal() {
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <TouchableOpacity
                onPress={() => navigation.navigate("Count")}
                style={styles.Botoes}
            >
                <Text style={styles.Text}>Count</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => navigation.navigate("Carrosel")}
                style={styles.Botoes}
            >
                <Text style={styles.Text}>Carrosel</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
    Botoes: {
        margin: 10,
        width: 200,
        height: 80,
        backgroundColor: "black",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,

    },
    Text: {
        color: 'white',
        fontFamily: 'Roboto',
        fontSize: 26
    }
});
